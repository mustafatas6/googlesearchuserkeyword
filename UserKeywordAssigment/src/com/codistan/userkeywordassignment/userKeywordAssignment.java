package com.codistan.userkeywordassignment;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class userKeywordAssignment {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		Scanner myscanner= new Scanner(System.in);
		System.out.println("What do you wanna search?");
		String searchWord = myscanner.nextLine();
		
		WebElement searchBox = driver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
		searchBox.sendKeys(searchWord + Keys.ENTER);
		
		List <WebElement> allLinks = driver.findElements(By.xpath("//cite[@class='iUh30']"));
		
		for (int i = 0; i < 5; i++) {
			String link = allLinks.get(i).getText();
			System.out.println(link);
		}
		

	}

}
